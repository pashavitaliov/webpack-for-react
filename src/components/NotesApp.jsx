var React = require('react');
var NoteEditor = require('./NoteEditor.jsx');
var NotesGrid = require('./NotesGrid.jsx');
var NoteSearch = require('./NoteSearch.jsx');

require('./NotesApp.css');

var NotesApp = React.createClass({

  getInitialState: function() {
    return {
      notes: [],
      searchString: ''
    }
  },
  componentDidMount: function() {
    // считывание поле notes из localStorage
    var localNote = JSON.parse(localStorage.getItem('notes'));
    if ( localNote ) {
      // если что-то записано - обновляем сотояние компонента
      this.setState({ notes: localNote });
    }
  },
  componentDidUpdate: function() {
    this._updateLocalStorage();
  },

  handleNoteDelete: function(note) {
    var noteId = note.id;
    // создаст новый массив кроме удаляемого элемента
    var newNotes = this.state.notes.filter(function(note) {
      // если id текущего элемента и удаляемого равны, элемент на возвращается
      return note.id !== noteId;
    });
    this.setState({ notes: newNotes });
  },

  handleNoteAdd: function(newNote) {
    var newNotes = this.state.notes.slice();
    newNotes.unshift(newNote);
    // this.setState({searchString: ''});
    this.setState({ notes: newNotes });
  },

  handlerSearch: function(searchString){
    this.setState({searchString: searchString});
  },
  render: function() {
    return (

        <div className="notes-app">
          <NoteSearch onSearchNote={this.handlerSearch}/>
          <NoteEditor onNoteAdd={this.handleNoteAdd}/>
          <NotesGrid notes={this.getFilterNotes(this.state.notes, this.state.searchString)}
                    onNoteDelete={this.handleNoteDelete}/>
        </div>
    );
  },

  getFilterNotes: function(notes, stringSerach){
    if (stringSerach.length){
      var searchQuery = stringSerach.toLowerCase();
      return notes.filter(function(note){
        var searchValue = note.text.toLowerCase();
        return searchValue.indexOf(searchQuery) !== -1;
      })
    }
    return notes;
  },
  _updateLocalStorage: function() {
    var notes = JSON.stringify(this.state.notes);
    localStorage.setItem('notes', notes);
  }
});

module.exports = NotesApp;
