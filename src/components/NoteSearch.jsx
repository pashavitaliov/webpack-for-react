var React = require('react');

require('./NoteSearch.css');

var NoteSearch = React.createClass({
  getInitialState: function() {
    return{
      searchValue: ''
    }
  },
  handlerSearchInput: function(event){
    // this.setState({searchValue: event.target.value});
    this.props.onSearchNote(event.target.value);
  },
  render: function() {
    return (
        <input className='note-search' type="text"  onChange={this.handlerSearchInput}/>
    );
  }
});

module.exports = NoteSearch;
