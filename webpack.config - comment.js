const path = require('path');
const webpack = require('webpack');
// создает шаблон документа, встраивает в него js файл
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const removeEmpty = x => x.filter(y => !!y);

module.exports = {
  context: path.join(__dirname, 'src'),

  resolve: {
    extensions: ['.js', '.jsx']
  },

  entry: removeEmpty([
    // необходимые модули для работы React HMR
    'babel-polyfill',
    process.env.NODE_ENV !== 'production' ? 'react-hot-loader/patch' : undefined,
    // из-за webpack/hot/only-dev-server, собирался только на проде, поэтому исключил и прод и дев
    (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'development')  ? 'webpack/hot/only-dev-server' : undefined,
    process.env.NODE_ENV === 'production' ? './index.prod.jsx' : './index.dev.jsx',
  ]),

  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'index.js',
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            extends: path.join(__dirname, '.babelrc')
          }
        }
      }, {
        test: /\.(png|jpg|svg|ttf|eot|woof|woof2)$/,
        use: [{
          loader: 'file-loader',
          options: { name: '[path][name].[ext]' }
        }]
      }
    ]
  },

  plugins: removeEmpty([
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
    }),
    // объединяет все модули в один, для быстроты работы файла
    //   применяется к модуляем es6 котореые обрабатываются непосредственно webpack
    //   при использовании транспилятора (babel) необходимо отключить modules
    //   в данном случае в файле .babelrc -> modules: false
    new webpack.optimize.ModuleConcatenationPlugin(),
    process.env.NODE_ENV !== 'production' ? new HtmlWebpackPlugin({
      title: 'My App',
      template: path.join(__dirname, 'src', 'index.html'),
    }) : undefined,
    process.env.NODE_ENV !== 'production' ? new webpack.NamedModulesPlugin() : undefined,
    process.env.NODE_ENV !== 'production' ? new webpack.NoEmitOnErrorsPlugin() : undefined,
    new ExtractTextPlugin('[name].css', { allChunks: true, disable: process.env.NODE_ENV !== 'productions'})
  ])
}


if ( process.env.NODE_ENV !== 'production' ) {
  module.exports.module.rules.push(
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader', 'autoprefixer-loader?browsers=last 4 version']
      }
  );
} else {
  module.exports.module.rules.push(
      {
        test: /\.css?$/,
        include: path.resolve(__dirname, './src/components'),
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                minimize: true,
                sourceMap: true
              }
            },
            { loader: 'autoprefixer-loader?browsers=last 4 version' },
          ]
        })
      }
  );
}
